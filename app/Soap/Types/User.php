<?php

namespace App\Soap\Types;

use App\User as UserModel;

class User
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $lastname;

    /**
     * @var string
     */
    public $documentType;

    /**
     * @var string
     */
    public $document;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $password;

    /**
     * User constructor.
     *
     * @param UserModel $user
     */
    public function __construct(UserModel $user)
    {
        $this->id = $user->id;
        $this->name = $user->name;
        $this->lastname = $user->lastname;
        $this->documentType = $user->document_type;
        $this->document = $user->document;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->status = $user->status;
        $this->password = $user->password;
    }
}
