<?php

namespace App\Soap;

use App\Soap\Types\User as UserType;
use App\User;
use Illuminate\Support\Facades\DB;

class Provider
{
    /**
     * Returns boolean status flag for given user and password.
     *
     * @param string $user
     * @param string $password
     * @return bool
     */
    public static function validateUser($user, $password)
    {
        return ($user == config('zoap.mock.user')) && ($password == config('zoap.mock.password'));
    }

    /**
     * Returns token for given user.
     *
     * @param string $user
     * @return string
     */
    public static function getToken($user)
    {
        return ($user == config('zoap.mock.user')) ? config('zoap.mock.token') : '';
    }

    /**
     * Returns boolean status flag for given token string.
     *
     * @param string $token
     * @return bool
     */
    public static function validateToken($token)
    {
        return ($token == config('zoap.mock.token'));
    }

    /**
     * Returns true if a user exists with given token or user and password.
     *
     * @param string $token
     * @param string $user
     * @param string $password
     * @return bool
     */
    public static function authenticate($token = '', $user = '', $password = '')
    {
        $result = false;

        if ($token) {
            $result = self::validateToken($token);
        } elseif ($user && $password) {
            $result = self::validateUser($user, $password);
        }

        return $result;
    }

    /**
     * Returns user by id.
     *
     * @param $userId
     * @return UserType
     */
    public static function findUser($userId)
    {
        $user = User::find($userId);
        return new UserType($user);
    }

    /**
     * Returns array of users by search criteria.
     *
     * @param array $criteria
     * @return UserType[]
     */
    public static function findUsersBy($criteria = [])
    {

        $users = DB::table('users')->where('id', '>', '9');

        if (array_key_exists('name', $criteria)) {
            $users->orWhere('name', 'like', '%' . $criteria['name'] . '%');
        }

        if (array_key_exists('lastname', $criteria)) {
            $users->orWhere('lastname', 'like', '%' . $criteria['lastname'] . '%');
        }

        if (array_key_exists('email', $criteria)) {
            $users->orWhere('email', 'like', '%' . $criteria['email'] . '%');
        }

        $users = $users->get();

        $usersType = [];

        foreach ($users as $user) {
            $userTemp = new User();

            $userTemp->name = $user->name;
            $userTemp->lastname = $user->lastname;
            $userTemp->document_type = $user->document_type;
            $userTemp->document = $user->document;
            $userTemp->email = $user->email;
            $userTemp->phone = $user->phone;
            $userTemp->status = $user->status;
            $userTemp->password = $user->password;

            $usersType[] = new UserType($userTemp);
        }

        return $usersType;
    }

    /**
     * @param $name
     * @param $lastname
     * @param $documentType
     * @param $document
     * @param $email
     * @param $phone
     * @param $status
     * @param $password
     * @return int
     */
    public static function createUser($name, $lastname, $documentType, $document, $email, $phone, $status, $password)
    {
        $user = new User;
        $user->name = $name;
        $user->lastname = $lastname;
        $user->document_type = $documentType;
        $user->document = $document;
        $user->email = $email;
        $user->phone = $phone;
        $user->status = $status;
        $user->password = $password;
        $user->save();

        return $user->id;
    }
}
