<?php

namespace App\Soap;

//use App\Soap\Types\KeyValue;
//use App\Soap\Types\User;
use SoapFault;
use App\Soap\Provider as Provider;

class Service
{
    /**
     * Authenticates user/password, returning status of true with token, or throws SoapFault.
     *
     * @param string $user
     * @param string $password
     * @return array
     * @throws SoapFault
     */
    public function auth($user, $password)
    {
        if (Provider::validateUser($user, $password)) {
            return ['status' => 'true', 'token' => Provider::getToken($user)];
        } else {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
    }

    /**
     * Returns a user by id.
     *
     * @param int $userId
     * @param string $token
     * @param string $user
     * @param string $password
     * @return App\Soap\Types\User
     * @throws SoapFault
     */
    public function getUser($userId, $token = '', $user = '', $password = '')
    {
        if (! $userId) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify user id.');
        }

        if (! Provider::authenticate($token, $user, $password)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

        return Provider::findUser($userId);
    }

    /**
     * Returns an array of users by search criteria.
     *
     * @param App\Soap\Types\KeyValue[] $criteria
     * @param string $token
     * @param string $user
     * @param string $password
     * @return App\Soap\Types\User[]
     * @throws SoapFault
     */
    public function getUsers($criteria = [], $token = '', $user = '', $password = '')
    {
        if (! Provider::authenticate($token, $user, $password)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

        return Provider::findUsersBy(self::arrayOfKeyValueToArray($criteria));
    }

    /**
     * @param string $name
     * @param string $lastname
     * @param string $documentType
     * @param string $document
     * @param string $email
     * @param string $phone
     * @param string $status
     * @param string $pass
     * @param string $token
     * @param string $user
     * @param string $password
     * @return int
     * @throws SoapFault
     */
    public function createUser($name, $lastname, $documentType, $document, $email, $phone, $status, $pass, $token = '', $user = '', $password = '')
    {
        if (! Provider::authenticate($token, $user, $password)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

        return Provider::createUser($name, $lastname, $documentType, $document, $email, $phone, $status, $pass);
    }

    /**
     * Convert array of KeyValue objects to associative array, non-recursively.
     *
     * @param App\Soap\Types\KeyValue[] $objects
     * @return array
     */
    protected static function arrayOfKeyValueToArray($objects)
    {
        $return = array();
        foreach ($objects as $object) {
            $return[$object->key] = $object->value;
        }

        return $return;
    }
}
