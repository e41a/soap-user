<?php

return [

    // Service configurations.

    'services' => [
        'test' => [
            'name' => 'User',
            'class' => 'App\Soap\Service',
            'exceptions' => [
                'Exception'
            ],
            'types' => [
                'keyValue' => 'App\Soap\Types\KeyValue',
                'user' => 'App\Soap\Types\User'
            ],
            'strategy' => 'ArrayOfTypeComplex',
            'headers' => [
                'Cache-Control' => 'no-cache, no-store'
            ],
            'options' => []
        ]
    ],


    // Log exception trace stack?

    'logging' => true,


    // Mock credentials for demo.

    'mock' => [
        'user' => 'demo@soap.com',
        'password' => 'demo',
        'token' => 'tGSGYv8al1Ce6Rui8oa4Kjo8ADhYvR9x8KFZOeEGWgU1iscF7N2tUnI3t9bX'
    ],
];
